package com.example.ali.buttoncounter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private static final String TEXT_VIEW_STATE = "textViewState";

    Button submit;
    TextView showText;
    EditText input;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        submit = (Button) findViewById(R.id.submit);
        input = (EditText) findViewById(R.id.input);
        showText = (TextView) findViewById(R.id.showText);

        showText.setMovementMethod(new ScrollingMovementMethod());

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inputText = input.getText().toString().trim();
                showText.append(inputText + "\n");
                input.setText("");
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(TEXT_VIEW_STATE , showText.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String textViewFromBundle = savedInstanceState.getString(TEXT_VIEW_STATE);
        showText.setText(textViewFromBundle);
    }
}
